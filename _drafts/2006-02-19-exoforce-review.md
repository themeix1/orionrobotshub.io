---
created: 2006-02-19 08:04:30
tags: [lego, exoforce, mecha, fiction]
title: Lego Exoforce Models Review
layout: post
---
This weekend, while out in Tesco, I spotted the two human Exoforce models Takeshi and Hikaru. As they were on offer at £15 for the pair, I snapped them up.

Now, for the uninitiated, Exoforce is Lego's current line of Mecha based models, on a theme of a humans-vs-machines war. Lots of cheesy (but cool) anime mecha references, as well as new Lego guys with warped, trigger happy battle snarls and vivid dayglo extreme manga hair. The Lego minifigs aren't all smiley nice guys any more, clearly.

These mechas are not too much of a challenge to build, but are definately worth it. The biggest challenges to them are feeding the light tubes through to the weapons, which took a fair bit of patience and manual dexterity. As it is one of the last things to do in the assmebly of the mecha, you get that horrifying moment every Lego builder gets where they try to work out how they are going to apply the required force in one area, without the whole model going to peices in their hands. Luckily I managed both mechas without incident.

There are a couple of other gripes though, the first being that the 7700 "Dragon Wing" Hikaru set does not actually have the peice in the kit list from the build guide. You may find that you need to substitute the grey pins depicted in the construction guide for technic black friction pins - no big problem, but Lego should watch that.

Another gripe is the Hollywood house fronts syndrome. These mechas look great from the front, and to some degree from the sides, but tthey have no detail from the back. If you look at my [blog-post?mad|"MAD-CM1"] mech, or the amazing efforts of the (Lugnet.build.mecha)[http://news.lugnet.com/build/mecha] guys, we do spend time on making a model that looks like it has a back as well.

One remaining gripe is the sticker sheets. I think the decal looks great, and I am sure kids appretiate stickers, but I often find it a fiddly pain trying to get them on the bricks straight, and not slightly off-center. Maybe it is just me but I would rather brick prints. At least the did not do the loathed stickers-over-multiple-bricks thing they did with the Nitro-Menace.

The light-bricks and tubes are coool, although I do wander about the battery length, and how to change the cell when they run down, as they inevitably well. Being LED's it would not be for some time, but equally, as kids play with them, parents must be aware of the possibility of old batteries leaking when unused - which could be a major hazaard.

The canopy on Hikaru was a little too much of a loose fit. Both models are very easy to pose, and make relatively stable bipeds. The "Dragon Fang" (Takeshi's Mech) hides the joints a bit better, and seems to have been deigned after the "Dragon Wing" mecha - must notably because Wing has an ugly exposed bit of tile in the hip area.

The dragons fang pincer weapon arm should maybe be on a click hinge, as in its current form it is not stiff enough and tends to flop.

In summary though, I have to say they are two great models, good fun to build, and they do add some interesting peices to a collection. I would heartily recommend picking them up in Tesco while they are still on offer.