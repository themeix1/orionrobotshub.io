---
layout: post
title: "Raspberry Pi Pico Wireless pack and Motors"
description: "Using CircuitPython, Raspberry Pi Pico, Pimoroni wireless Pack and Kitronic Motor Board Together"
category: "robot building"
tags: [raspberrry pi pico, robot building, electronics, esp32, python, circuitpython]
gallery:
    - file: <first>.jpg
      title: <jpg description>
asset_dir: galleries/<year>/<mm-dd-post name>
thumb: /galleries/<year>/<mm-dd-post name>/thumbnails/<first>.jpg
---