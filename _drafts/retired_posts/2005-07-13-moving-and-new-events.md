---
created: 2005-07-13 09:56:48
description: Mostly offline, Moving, New Study, Task Management and Birthdays
tags: [london, clubs, events, lego, mindstorms]
title: Mostly offline, Moving, New Study, Task Management and Birthdays
layout: post
---
Hi all,

I am now currently mostly offline at the moment. I have now moved to our new place - whew. What a big job.

Between the unpleasant events of last week - which nearly lead to my ride pulling out (close friends with a van to help me move),
and the actual move as well as the fact that it has been a blisteringly hot couple of days -
I have had a full on weekend.

The annoying thing is that it is at least a week before I get broadband internet access at home. Net cafes have woefully inadequate machines - although it did highlight how difficult my website can be to use on very low-res computers.

I have also noted that the recent update to the wiki software has left the title bars in the blogs unreadable - I will be messing about with the CSS for this when I get a chance - that's a promise!

The upshot of all of this, is in my new place, I now have a dedicated study room - so that's a mini-workshop at home for when I have smaller stuff to do. It means that once net access is restored - I will be able to get more productivity. I will also be working on my task management app - which will be first previewed here - being of the coder/inventor mindset - when I really cannot find a tool that does what I want - I write my own!

Anyway - OrionRobots is about to shift into a new phase, where I will be getting involved in lots of youth events, including kids birthdays, after-school and groups like scouts, brownies and Woodcraft.
Please contact me - using @orionrobots on twitter, if you would like to book an event - these are all challenge based, mostly using Lego and similar to some of the content seen in the East Finchley Robotics Club.

Orion
