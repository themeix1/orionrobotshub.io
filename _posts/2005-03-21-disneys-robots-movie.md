---
created: 2005-03-21 05:10:53
description: Disneys "Robots" Movie
tags: [robots]
title: Disneys Robots Movie
layout: post
---
## Movie - Disney's Robots

Later, my wife, myself, Dale and his family went to see the new movie "Robots". It was a wonderful movie. It has lots of imagination. The robots themselves look great - and are wonderfully animated. The number of gadgets, gizmos and inventions are enough to make it a good movie, but the storyline is kind of sweet and compelling as well - although I am not sure about the musical aspects of the film.

The transport sequence is undoubtedly one of the coolest parts of the film, and is ample inspiration to anyone considering building a [Great Ball Contraption](/wiki/great_ball_contraption.html "Great Ball Contraption") module. The characters are cute and funny, and while the good guy/bad guy divide is clear cut - as you would expect in a family film, there are definitely some "in" jokes that only an adult would get. It sounds good too, with voices from the likes of  Hale Berry, Mel Brooks, Ewan McGregor and the ever amusing Robin Williams as Fender.

It is definitely more kids film than adult, and kids will go for it. Anybody into gadgety stuff will appreciate the visuals - especially the way all the robots look like they articulate properly at all their joints and don't appear to be bending any way they shouldn't be. All in all - good fun.
