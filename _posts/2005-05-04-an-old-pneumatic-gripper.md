---
title: Pneumatic Lego Gripper
description: Some Lego creations of my own
tags: [robot building, robotics, mindstorms, pneumatic, rcx]
layout: post
---
With an imminent move planned, a number of old Lego builds needed to be taken apart, so I got photos of them all.

# Lego Gripper

I made a simple pneumatically operated Lego gripper.

![](/galleries/2005-05-04-an-old-pneumatic-gripper/p1010001.jpg){:class="img-responsive"}

The gripper used a single mini double acting cylinder and angled beam parts. I used 8t gears for some actual grip at the fingertips, although the design works better with a scooping action.

![](/galleries/2005-05-04-an-old-pneumatic-gripper/p1010002.jpg){:class="img-responsive"}
![](/galleries/2005-05-04-an-old-pneumatic-gripper/p1010003.jpg){:class="img-responsive"}

It is controlled (for demonstration purposes) by a single Lego valve and basic pump. Perhaps the gripper would be better if they were separated by longer pneumatic pipes, otherwise the mechanism looks bulkier.

# Light sensor head

I did a study using a geared down motor with a clutch gear, and a light sensor to make a head. 
The idea was that the rear liftarm would stop it going outside a preset range, and a program would scan periodically side-to-side, and attempt to judge which area was brightest.

![](/galleries/2005-05-04-an-old-pneumatic-gripper/p1010004.jpg){:class="img-responsive"}

<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=GB&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=orionrobots-21&language=en_GB&marketplace=amazon&region=GB&placement=B082WD5YV9&asins=B082WD5YV9&linkId=beb70788ccaaea84a7820473034e4cd9&show_border=true&link_opens_in_new_window=true"></iframe>