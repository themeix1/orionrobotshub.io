---
created: 2005-06-06 10:23:32
description: More on the signal generator
tags: [electronics]
title: More on the signal generator
layout: post
todo_needs_sat: recreate gallery from Picasa.
gal: /galleries/gallery-8-signal-generator-diode-bridge/
---
# Signal Generator build

![]({{page.gal}}/258-workshop.jpg){: .img-responsive .center-block}

When I [built the Diode bridge]({% post_url 2005-05-30-freeforming-a-rectifier-bridge %}) - I needed to generate signals to test it. This is how I built the Signal Generator I used to test them.

I managed to get the cavities for the system drilled into the case that came with the velleman amp kit. It was a bit awkward - the material the case is made out of is thin, and tends to split when drilled at all but the lowest speed setting. Equally, the material is hard to clamp effectively so it either cant be held firmly, or it splits.

When designing it, I did not manage to find all the dimensions for the socket parts, so I simply measured them up, and put holes to mount them in the CAD design. Once it all works, I will post the design here.

The DC power supply hole was an awkward shaped one - an elongated D with two small screw holes either side (a little too close). It required a fair amount of time with a Dremel, and then some elbow grease with a set of needle files to get it right. If you are going to attempt this yourself - be warned, that was the part I found most awkward. I am still debating on how I will mount the boards inside it.

I used the result to test the diode bridge.

![]({{page.gal}}/312-clean-result.jpg){: .img-responsive .center-block}

[Gallery showing all images]({{page.gal}})

