---
created: 2006-04-08 07:17:45
description: Amazing Robot video
tags: [robot, robotics]
title: Amazing Robot video
layout: post
---
Among the robots I have seen, few have been the crowd pleaser that this little creature known as "Layered-X" (AKA Rayered X) developed by Asurada and shown off to a wave of applause at the Robo-One competition in Tokyo in March of this year.

{% assign youtube_id="0m_cCP1wn94" %}
{% assign description="Robo-One 9: Robot Competition - LAYERED-X" %}
{% include youtube_link.html %}

You only need to see it perform here to understand why. It walks, it dances, it transforms, and its legs can become arms and vice versa. It shows a talent of engineering, and control that is definitely exceptional. All of this from something which is just a bundle of simple servos and linkages. Top stuff!

## Links

* <http://www.asurada.jp/> - Asurada's website on RAYERD-X (Its real name)
