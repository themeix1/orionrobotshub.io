---
created: 2006-05-27 17:25:08
description: BCS Run poll and story about robots and games in education
tags: [robotics, computing]
title: BCS Run poll and story about robots and games in education
layout: post
---
{% include JB/setup %}

The [British Computer Society](http://www.bcs.org) are running a story and a poll on whether building robots and programming computer games should be used to encourage learning in 14-19 year olds. The article has quite some depth, and covers two of my major passions (I have been a pro game developer myself).

Go read it, and vote!

[Wanted: ways to engage young people with IT - Go Read the story](http://www.bcs.org/server.php?show=ConWebDoc.4427)
