---
created: 2008-02-06T09:10:01.000Z
description: Magnetically Linking Robots
tags:
  - robots
  - nanobots
  - swarmbots
  - clusterbots
  - swarm
  - nanotech
title: Magnetically Linking Robots
layout: post
---

Among the things mentioned occasionally on OrionRobots are Swarm Bots, [Nano Bots](/wiki/nano_bots.html "Microscopic or Nano scale robots") and [cluster Bots](/wiki/cluster_bots.html).

One cool idea among these is that the robots can join together in some movable arrangement to perform tasks - to cluster together. Of course, while taking into account lessons from SciFi (like The Replicators who do just this), they could be very handy for all kinds of things, including rapid prototyping, bridge forming, creating robotic manipulators and other stuff.

Now having real ones on Nano Scale is still quite a distance away, however, a team at [Carnegie Mellon University](/wiki/carnegie_mellon_university.html "An institution involved in Robotics, Technology and Science") led by Seth Goldstein have come up with some demonstration robots which use a ring of electromagnets around their perimeter to bind and move around each other. The goal is to create the microscopic swarming robots eventually, and small electrical charges instead of magnets are likely to be more efficient at that scale.

These demonstrations of robots has been dubbed "claytronic". NewScientist created a video of this demo in action.

<iframe width="420" height="315" src="//www.youtube.com/embed/e44hA6IBtkA?rel=0" frameborder="0" allowfullscreen="true">
</iframe>

## Links

- [NewsScientistTech : Shape-shifting robot forms from magnetic swarm](https://www.newscientist.com/article/dn13244-shape-shifting-robot-forms-from-magnetic-swarm/)
- [Engadget : Shape-shifting magnetic bots take a page out of the Dharma playbook](http://www.engadget.com/2008/01/29/shape-shifting-magnetic-bots-take-a-page-out-of-the-dharma-playb/)
