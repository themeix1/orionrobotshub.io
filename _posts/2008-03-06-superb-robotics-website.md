---
created: 2008-03-06 14:29:56
description: Superb Robotics Website
tags: [robot, sensors, motors, robot building, microcontrollers, electronics, measurement, robots]
title: Superb Robotics Website
layout: post
---
I have now found another good resource with plenty of information on robotics and building robots. The content on the site is largely original, and the site has a visitor contribution area as well as a number of forums. It is also relatively recent.

The articles are very instructional with plenty of diagrams and the site is very easy to navigate. There are detailed circuit diagrams, sections of code (programs) and discussion on the topics.

So I present to you Ikalogic - <http://www.ikalogic.com> where you can find information on Robot Navigation, Micro-controllers, Motor Control, General Electronics and Sensors and Measurement.
