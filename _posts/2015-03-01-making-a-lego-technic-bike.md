---
layout: post
title: Making A lego Technic Bike
tags: [lego, technic, stem,video]
---
This week I built a Lego Technic bike after one of my children asked me how a bicycle worked. Watch below to see it demonstrated and how I put it together.

<div class="embed-responsive embed-responsive-16by9">
<iframe width="560" height="315" src="https://www.youtube.com/embed/ij74-jHRHtk?rel=0" frameborder="0" allowfullscreen="true"></iframe>
</div>
