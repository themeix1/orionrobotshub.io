---
layout: post
title: "Piwars 2020 The Roof Rack"
description: "3D Printed Technic Lego Adaptor For Raspberry Pi Camera"
tags: [robotics, raspberry pi, raspberry pi projects, raspberry pi 3 projects, piwars, robot, raspberry pi zero w projects, piwars, fusion360, 3d printing, technic lego, camera]
---
{% include JB/setup %}

## The State of The Piwars 2020 Robot

When this video was recorded, I was building the camera mount for the robot. I'd realised I'd need to adapt the robot to different situations, and I wanted to make it field
adaptable.

I'd attempted a pantograph type folding assembly, sliding carriage types - all in my head and on paper. None were quite right. My intent was not to need a screwdriver to
make these changes in the field, and just use a few choice Lego parts to make the different modes the robot could be in.

I used Fusion 360 to prototype these ideas, and printed the first attempts at friction fitting, something that was not Lego, and came to the conclusion that making something Lego compatible with the Technic system would be easier and give me far more flexibility. It means I can consider Lego for building more complex mechanisms and interfacing them with my robots. Mixed mode.

I designed and made an adaptor for Lego Technic Pins that bolts to the top of my lunch box. I printed this on my Flashforge Finder 3D Printer.
I nicknamed this part The Roof rack. And it works a treat.

See the video below for more detail.

<div class="embed-responsive embed-responsive-16by9">
<iframe width="560" height="315" src="https://www.youtube.com/embed/xEnxea_wBOc" frameborder="0" allowfullscreen="True"></iframe>
</div>
