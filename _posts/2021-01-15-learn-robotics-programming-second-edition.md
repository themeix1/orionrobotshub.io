---
layout: post
title: "Learn Robotics Programming second edition"
description: "The release of Learn Robotics Programming, second edition"
tags: [robotics, raspberry pi, raspberry pi projects, raspberry pi 3 projects, raspberry pi 4 projects, robot, electronics, computer vision, voice recognition]
---
Hello, I've some great news.

On February 12th, 2021, Learn Robotics Programming 2nd Edition will be released and is now on pre-order!

[![Learn robotics Programming 2nd Edition Cover](/galleries/learn-robotics-programming-2nd-edition/learn-robotics-programming-2nd-cover.jpg)](http://packt.live/2XccaKe)

This is a step-by-step guide to building and programming a #RaspberryPi #python #robot. Pick up #robotics fundamentals.

Get into more advanced robotics with Inertial Measurement Units (compass and orientation) - this robot knows which way is up.

Use sensors to avoid objects. Control a robot with your phone and see through its camera.

Build a robot with ultrasonic sensors to detect distances, to detect how far the robot has travelled and a camera. Learn to use computer vision to sense coloured objects and drive towards them, or track faces and point its "head" at them. See how to write code to drive the robot with your phone, make it avoid walls, follow lines and make a voice recognition assistant to control the robot by speaking.

All done in python 3 on the Raspberry Pi.
Build a voice agent to control the robot. Interact using an #OpenCV enabled camera for face recognition, colored object tracking and line following.

Tags: #engineering #technology #artificialintelligence #automation

Links to order: <http://packt.live/2XccaKe>
UK: <https://www.amazon.co.uk/Learn-Robotics.../dp/1839218800>
