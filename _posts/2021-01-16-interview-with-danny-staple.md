---
layout: post
title: "Interview with Danny Staple"
description: "Interview on Learn Robotics Programming, second edition"
tags: [robotics, raspberry pi, raspberry pi projects, raspberry pi 3 projects, raspberry pi 4 projects, robot, electronics, computer vision, voice recognition]
---
This week I was interviewed by Packt Publishing, the publisher of [Learn Robotics Programming second Edition](http://packt.live/2XccaKe), on how my experience with writing the book has been.

Come and see my interview at <https://authors.packtpub.com/interview-with-danny-staple/>.

In other news, I am planning a video on connecting a robot arm to a web page with a Raspberry Pi. More to come when I've filmed it.
