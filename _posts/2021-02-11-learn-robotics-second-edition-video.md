---
layout: post
title: "Learn Robotics Programming Second Edition Video Announcement"
description: "Announcement Video For Learn Robotics Programming Second Edition"
category: 
tags: [robotics, raspberry pi, raspberry pi projects, raspberry pi 3 projects, raspberry pi 4 projects, robot, electronics, computer vision, voice recognition]
---
{% include JB/setup %}

Further to my recent announcements about the publication of my book, [Learn Robotics Programming second edition](http://packt.live/2XccaKe) I have made a video talking about the books features, showing demonstrations of the robot you'll build in action.

<div class="embed-responsive embed-responsive-16by9">
<iframe width="560" height="315" src="https://www.youtube.com/embed/wCL8LrQ8RcA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="true"></iframe>
</div>

This is the second best place to see that Raspberry Pi based Python3 robot tracking and following coloured objects, tracking faces, sonar scans, avoiding obstacles and driven from a phone.

The first best place is to buy the book and build one yourself! Where you'll be able to do speech recognition, driving fixed distances, line following and more.
