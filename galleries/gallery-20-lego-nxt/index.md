---
description: Photos relating to the mindstorms NXT system
images:
- {caption: The first robot from the start here box in the home NXT kit. With an added
    Ultrasonic sensor., date: !!timestamp '2007-01-10 14:17:11', name: StartHereBot.jpg,
  src: !!python/unicode '409-p1010057.JPG', thumb-src: !!python/unicode 'thm-409-49x80-startherebot.jpg'}
- {caption: '', date: !!timestamp '2007-01-13 08:04:59', name: BotWithTouchSensor.JPG,
  src: !!python/unicode '410-botwithtouchsensor.JPG', thumb-src: !!python/unicode 'thm-410-80x68-botwithtouchsensor.JPG'}
- {caption: '', date: !!timestamp '2007-01-13 08:04:59', name: Lego45590.JPG, src: !!python/unicode '411-lego45590.JPG',
  thumb-src: !!python/unicode 'thm-411-80x67-lego45590.JPG'}
- {caption: '', date: !!timestamp '2007-01-13 08:04:59', name: TeaAndRobots.JPG, src: !!python/unicode '412-teaandrobots.JPG',
  thumb-src: !!python/unicode 'thm-412-80x73-teaandrobots.JPG'}
- {caption: '', date: !!timestamp '2007-01-18 12:41:21', name: SimpleGrabberProgram.png,
  src: !!python/unicode '413-simplegrabberprogram.png', thumb-src: !!python/unicode 'thm-413-80x17-simplegrabberprogram.png'}
- {caption: '', date: !!timestamp '2007-01-18 12:41:21', name: BotWithSoundSensor.JPG,
  src: !!python/unicode '414-botwithsoundsensor.JPG', thumb-src: !!python/unicode 'thm-414-75x80-botwithsoundsensor.JPG'}
- {caption: '', date: !!timestamp '2007-01-18 12:41:21', name: BallGrabbed.JPG, src: !!python/unicode '415-ballgrabbed.JPG',
  thumb-src: !!python/unicode 'thm-415-80x63-ballgrabbed.JPG'}
- {caption: '', date: !!timestamp '2007-01-18 12:41:21', name: P1010026.JPG, src: !!python/unicode '416-p1010026.JPG',
  thumb-src: !!python/unicode 'thm-416-80x60-p1010026.JPG'}
- {caption: '', date: !!timestamp '2007-01-18 12:41:21', name: P1010027.JPG, src: !!python/unicode '417-p1010027.JPG',
  thumb-src: !!python/unicode 'thm-417-80x60-p1010027.JPG'}
- {caption: '', date: !!timestamp '2007-01-18 14:57:18', name: P1010026WithInset.JPG,
  src: !!python/unicode '418-p1010026withinset.JPG', thumb-src: !!python/unicode 'thm-418-80x60-p1010026withinset.JPG'}
- {caption: '', date: !!timestamp '2007-01-23 15:16:50', name: P1010002-1.JPG, src: !!python/unicode '419-p1010002-1.JPG',
  thumb-src: !!python/unicode 'thm-419-80x49-p1010002-1.JPG'}
- {caption: '', date: !!timestamp '2007-04-08 15:36:49', name: P1010006.JPG, src: !!python/unicode '445-p1010006.JPG',
  thumb-src: !!python/unicode 'thm-445-76x80-p1010006.JPG'}
- {caption: '', date: !!timestamp '2007-04-08 15:36:49', name: P1010007.JPG, src: !!python/unicode '446-p1010007.JPG',
  thumb-src: !!python/unicode 'thm-446-80x71-p1010007.JPG'}
- {caption: '', date: !!timestamp '2007-04-08 15:38:51', name: P1010006-2.jpg, src: !!python/unicode '447-p1010006-2.jpg',
  thumb-src: !!python/unicode 'thm-447-76x80-p1010006-2.jpg'}
- {caption: '', date: !!timestamp '2007-04-08 15:38:51', name: P1010007-2.JPG, src: !!python/unicode '448-p1010007-2.JPG',
  thumb-src: !!python/unicode 'thm-448-80x71-p1010007-2.JPG'}
layout: autogallery
name: Lego NXT
