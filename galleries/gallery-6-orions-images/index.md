---
description: Images I use in my Blog, and maybe elsewhere.
images:
- {caption: '', date: !!timestamp '2007-02-04 01:29:41', name: jimgray.gif, src: !!python/unicode '427-jimgray.gif',
  thumb-src: !!python/unicode 'thm-427-48x80-jimgray.gif'}
- {caption: Photo of the new Hitachi Robot, date: !!timestamp '2005-03-17 07:09:11',
  name: robot_hitachi203afp.jpg, src: !!python/unicode '187-robot-hitachi203afp.jpg',
  thumb-src: !!python/unicode 'thm-187-54x80-robot-hitachi203afp.jpg'}
- {caption: '', date: !!timestamp '2005-03-17 07:09:25', name: HitachiRobot2.jpg,
  src: !!python/unicode '188-hitachirobot2.jpg', thumb-src: !!python/unicode 'thm-188-66x80-hitachirobot2.jpg'}
- {caption: '', date: !!timestamp '2005-03-17 07:09:35', name: HitachiRobotAFP.jpg,
  src: !!python/unicode '189-hitachirobotafp.jpg', thumb-src: !!python/unicode 'thm-189-59x80-hitachirobotafp.jpg'}
- {caption: "Stanley, Standford Universities Champion of the 2005 grand Challenge,\
    \ Stands proud at the NQE event preceding the race.\r\n\r\nNote the array of sensory\
    \ gear on the rack over the roof.", date: !!timestamp '2005-10-08 16:38:02', name: Stanley.jpg,
  src: !!python/unicode '321-stanley-nqe1-1.jpg', thumb-src: !!python/unicode 'thm-321-74x80-stanley.jpg'}
- {caption: A picture of me. Currently standing in front of Neushwanstein in Germany.,
  date: !!timestamp '2005-12-11 16:03:54', name: Danny.jpg, src: !!python/unicode '326-danny.jpg',
  thumb-src: !!python/unicode 'thm-326-73x80-danny.jpg'}
- {caption: '', date: !!timestamp '2005-12-11 16:05:38', name: Danny1.jpg, src: !!python/unicode '327-danny1.jpg',
  thumb-src: !!python/unicode 'thm-327-74x80-danny1.jpg'}
- {caption: A robot built based upon the Lego NXT system., date: !!timestamp '2006-01-05
    18:32:07', name: NXTImage.jpg, src: !!python/unicode '356-nxtimage.jpg', thumb-src: !!python/unicode 'thm-356-62x80-nxtimage.jpg'}
- {caption: The Lego RCX, date: !!timestamp '2006-01-05 18:42:08', name: RCXImage.jpg,
  src: !!python/unicode '357-rcximage.jpg', thumb-src: !!python/unicode 'thm-357-76x80-rcximage.jpg'}
- {caption: An Arm based upon the new Mindstorms NXT., date: !!timestamp '2006-01-05
    18:49:42', name: MindstormsNXTArm.jpg, src: !!python/unicode '358-mindstormsnxtarm.jpg',
  thumb-src: !!python/unicode 'thm-358-80x72-mindstormsnxtarm.jpg'}
- {caption: The NXT With an array of sensors, date: !!timestamp '2006-01-05 18:51:21',
  name: MindstormsNXT.jpg, src: !!python/unicode '359-mindstormsnxt.jpg', thumb-src: !!python/unicode 'thm-359-80x60-mindstormsnxt.jpg'}
- {caption: More images from the Lego NXT press release, date: !!timestamp '2006-01-09
    06:12:23', name: ElbowJoint.jpg, src: !!python/unicode '360-elbowjoint.jpg', thumb-src: !!python/unicode 'thm-360-80x52-elbowjoint.jpg'}
- {caption: Careful - this is big!, date: !!timestamp '2006-06-30 15:05:06', name: tikiwiki-mods-config-screenshot.png,
  src: !!python/unicode '380-tikiwiki-mods-config-screenshot.png', thumb-src: !!python/unicode 'thm-380-80x51-tikiwiki-mods-config-screenshot.png'}
- {caption: '', date: !!timestamp '2006-07-08 04:04:26', name: wuyulurobot.jpg, src: !!python/unicode '383-wuyulurobot.jpg',
  thumb-src: !!python/unicode 'thm-383-80x80-wuyulurobot.jpg'}
- {caption: '', date: !!timestamp '2006-07-08 04:21:24', name: Wu-Yulu-and-his-robot-driven-rickshaw.jpg,
  src: !!python/unicode '384-wu-yulu-and-his-robot-driven-rickshaw.jpg', thumb-src: !!python/unicode 'thm-384-80x72-wu-yulu-and-his-robot-driven-rickshaw.jpg'}
- {caption: '', date: !!timestamp '2006-08-22 15:44:20', name: gp2xbob.jpg, src: !!python/unicode '387-gp2xbob.jpg',
  thumb-src: !!python/unicode 'thm-387-80x60-gp2xbob.jpg'}
- {caption: '', date: !!timestamp '2006-08-22 15:56:29', name: gp2x.gif, src: !!python/unicode '388-gp2x.gif',
  thumb-src: !!python/unicode 'thm-388-80x10-gp2x.gif'}
- {caption: 'PicoCricket Code Sample, courtesy of PicoCricket.com', date: !!timestamp '2006-08-31
    02:02:12', name: press-sample-code-thumb.gif, src: !!python/unicode '390-press-sample-code-thumb.gif',
  thumb-src: !!python/unicode 'thm-390-64x80-press-sample-code-thumb.gif'}
- {caption: '', date: !!timestamp '2006-08-31 02:59:41', name: picocricket.gif, src: !!python/unicode '391-picocricket.gif',
  thumb-src: !!python/unicode 'thm-391-64x80-picocricket.gif'}
- {caption: '', date: !!timestamp '2007-03-09 14:03:52', name: minibot.jpg, src: !!python/unicode '434-minibot.jpg',
  thumb-src: !!python/unicode 'thm-434-minibot.jpg'}
- {caption: Bot strapped together at 24C3., date: !!timestamp '2008-01-27 14:33:56',
  name: 1-18-08-robochumby.jpg, src: !!python/unicode '453-1-18-08-robochumby.jpg',
  thumb-src: !!python/unicode 'thm-453-1-18-08-robochumby.jpg'}
- {caption: '', date: !!timestamp '2008-03-30 11:29:09', name: AshShell.png, src: !!python/unicode '454-ashshell.png',
  thumb-src: !!python/unicode 'thm-454-80x32-ashshell.png'}
- {caption: '', date: !!timestamp '2008-03-30 11:29:09', name: LsAshShell.png, src: !!python/unicode '455-lsashshell.png',
  thumb-src: !!python/unicode 'thm-455-80x16-lsashshell.png'}
- {caption: '', date: !!timestamp '2008-03-30 11:29:09', name: PhoneNetworkService.png,
  src: !!python/unicode '456-phonenetworkservice.png', thumb-src: !!python/unicode 'thm-456-80x24-phonenetworkservice.png'}
- {caption: '', date: !!timestamp '2008-03-30 11:29:09', name: PhoneNetworkServiceUsers.png,
  src: !!python/unicode '457-phonenetworkserviceusers.png', thumb-src: !!python/unicode 'thm-457-80x18-phonenetworkserviceusers.png'}
- {caption: '', date: !!timestamp '2008-03-30 11:29:09', name: PuttySession.png, src: !!python/unicode '458-puttysession.png',
  thumb-src: !!python/unicode 'thm-458-80x77-puttysession.png'}
- {caption: '', date: !!timestamp '2008-03-30 11:29:09', name: TelnetLoginScreen.png,
  src: !!python/unicode '459-telnetloginscreen.png', thumb-src: !!python/unicode 'thm-459-80x31-telnetloginscreen.png'}
- {caption: '', date: !!timestamp '2008-03-30 11:29:09', name: TelnetNetworkService.png,
  src: !!python/unicode '460-telnetnetworkservice.png', thumb-src: !!python/unicode 'thm-460-80x22-telnetnetworkservice.png'}
- {caption: '', date: !!timestamp '2008-03-30 11:29:09', name: UserApplication.png,
  src: !!python/unicode '461-userapplication.png', thumb-src: !!python/unicode 'thm-461-80x44-userapplication.png'}
- {caption: '', date: !!timestamp '2008-03-30 11:29:09', name: UserTelnetNetworkService.png,
  src: !!python/unicode '462-usertelnetnetworkservice.png', thumb-src: !!python/unicode 'thm-462-80x25-usertelnetnetworkservice.png'}
- {caption: '', date: !!timestamp '2009-01-02 13:34:50', name: Piriform Defraggler.png,
  src: !!python/unicode '598-piriform-defraggler.png', thumb-src: !!python/unicode 'thm-598-80x80-piriform-defraggler.png'}
- {caption: '', date: !!timestamp '2009-01-02 17:32:38', name: synergy.gif, src: !!python/unicode '599-synergy.gif',
  thumb-src: !!python/unicode 'thm-599-80x29-synergy.gif'}
- {caption: "A current ((The Daleks|DALEK)) seen in BBC TV Centre reception and taken\
    \ by user [Zir|http://en.wikipedia.org/wiki/User:Zir] for Wikipedia.\r\n\r\n[http://en.wikipedia.org/wiki/File:DALEK.jpg|Original\
    \ source]\r\n\r\nPermission is granted to copy, distribute and/or modify this\
    \ document under the terms of the [http://en.wikipedia.org/wiki/Wikipedia:Text_of_the_GNU_Free_Documentation_License|GNU\
    \ Free Documentation License], Version 1.2 or any later version published by the\
    \ Free Software Foundation; with no Invariant Sections, no Front-Cover Texts,\
    \ and no Back-Cover Texts. Subject to [http://en.wikipedia.org/wiki/Wikipedia:General_disclaimer|disclaimers].\r\
    \n\r\nThis work is licensed under the [http://en.wikipedia.org/wiki/Creative_Commons|Creative\
    \ Commons]  [http://creativecommons.org/licenses/by/2.5/|Attribution 2.5] License.",
  date: !!timestamp '2009-01-27 03:35:37', name: DALEK.jpg, src: !!python/unicode '600-dalek.jpg',
  thumb-src: !!python/unicode 'thm-600-60x80-dalek.jpg'}
layout: autogallery
name: Orions images
---