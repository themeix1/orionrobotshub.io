---
layout: page
tags: [electronics]
title: ADC
---
Abbreviation for Analog to Digital Converter.

These devices are commonly placed on input/sensor lines in [microcontrollers](/wiki/microcontroller.html) to allow continuously varying analog signals to be converted into binary signals.

For example, an 8 <a href="/wiki/bit.html" title="Binary Digit">bit</a> ADC would convert the power levels to one of 256 numbers. This can then be easily interpreted by a program.

It is using these, that the <a href="/wiki/rcx.html" title="The Lego RCX">Lego RCX</a> is able to have up to three light-sensors or similar devices connected. In the case of the <a href="/wiki/rcx.html" title="The Lego RCX">RCX</a> they have much better resolution than 8 bits, allowing for a more sensitive or greater range of variance.

This can go an opposite way by using <a href="/wiki/dac.html" title="Digital To Analog Converter">DACs</a>.

A classic Arduino like the Uno has 6 exposed analog pins using internal ADCs allowing you to connect analog sensors.
