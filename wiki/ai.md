---
layout: page
title: AI
date: 2005-03-16 15:57:34
---
AI is an abbreviation for Artificial Intelligence.

It is a broad covering definition of the concepts that may make a machine appear to be or actually be behaving intelligently.
There are many techniques which fall under this banner

* Machine Learning
    * Artificial Life
    * <a href="/wiki/genetic_algorithm.html" title="Genetic Algorithm">Genetic Algorithms</a>
    * Neural Networks
* Natural Language Processing
* Subsumption Architecture
* Expert Systems
* Visual Processing

A system does not necessarily have to learn to be considered AI, hence the distinct term Machine Learning.
