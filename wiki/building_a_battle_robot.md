---
layout: page
title: Building A Battle Robot
tags: [robots, robotics]
date: 2006-07-10 09:42:34
---
# Intro

So you want to be a contender eh? Not content with watching [Robot Wars](/wiki/robot_wars.html "The british robot smashing TV series.") and BattleBots - you want to get off your armchair and build your own! Well done. Now this is not going to be easy - but follow the right advice, and you will get there.

I am not going to deceive you, while it is fun, and almost anyone can achieve it, it really is not easy. You will need some hard work, some serious thinking and a great deal of determination. A lot of what I am going to say here is probably common sense, but collecting it and having it explicitly stated means that you are less likely to miss something.

You don't need a huge bank account, or deep pockets - as some of the best robots, like [SumpThing](/wiki/sumpthing.html "Sumpthing") are actually built from [salvage](/wiki/salvage_tips.html "Tips on pulling stuff apart to build robots. How, where and what.") and scrap. You might need an array of tools, but not a lot that couldn't be borrowed, and for a battle bot, the controllers can be kept fairly simple.

In the next few paragraphs, I will take you through the basic essentials that you will need, and later all the considerations you need when designing. You can then use the rest of the OrionRobots knowledge resources to get things built.

<h1  id="Get_a_team">Get a team</h1>
<p>
<br/>Unless you are an elite, bachelor(I mean single - not having a degree although that might help too) engineer with huge amounts of time and money on your hands, you are simply not likely to have the resources to build a serious robot as a one man band. Very few- if any of the battling robots were built this way - your first step is to find a likely team of builders. Feel free to skim the ads in our <a  href="http://orionrobots.co.uk/tiki-view_forum.php?forumId=6" rel="external" target="_blank">Robot Market Forum</a> or place your own to attract the team.
</p>
<p>A team could simply be some school friends, your local neighbourhood engineers, a biker club, an after school club - I am sure if you ask around a bit you can find someone interested.
</p>
<p>If your team members are under 18, I recommend at least one adult helper, who will have valuable advice and keep an eye on safety. Other than that proviso, generally anyone from age of 5 upwards could be helpful - even just for style and coming up with good ideas. Get a good mix of skills if you can, and a good mix of ages can usually help to provide that.
</p>
<p>Teams need not be all men (or boys), there are some pretty successful all-female teams, as well as mixed teams. Again - girls will help to introduce more varied skills and thinking to the team, adding to the inspiration and the potential of having all aspects considered.
</p>
<p>When you get people to join the team, you may want to appoint or become a leader, or at least a group coordinator, this does not mean being bossy, but more someone to martial ideas and to keep a little order and synchronisation in the teams activities. Make sure the team are aware of the kind of commitment they are going to need - half hearted maybes and no-shows have been the downfall of a few projects. Also, even if the event only allows for teams of a limited number, always have a few backups - understudies if you like, in case of someone being taken ill or running into problems.
</p>
<p>Team enthusiasm is important, so make sure you have it in good supply, and if you start to see people flagging/getting tired, that is the time to consider the causes and deal with them. Team friction is something best addressed as soon as possible. Sometimes just coming round with drinks can be enough to lighten everybody up. Make sure everyone takes breaks - but never nag them to take a break - if they are saying "give me a minute" - then give them that.
</p>
<p>Teams do have bad days, and tempers do fray - it is sometimes worth sitting down with the team and discussing a strategy to deal with those days beforehand, which may actually allow for early airing of any issues that could arise later.
</p>
<p>Coming up with a team name, and a logo or mascot or similar team motifs can be the first great team building exercise. Although it is generally considered a secondary task to the robotics, having something to rally around and something to show early can be a real motivator.
</p>
<h1  id="Preparation_-_Competition_Research">Preparation - Competition Research</h1>
<p>
<br/>Be ready to do a bit of research before building - there is no point in spending money on a fantastic contraption when it is disqualified straight away, or simply cannot compete for the correct event.
</p>
<p>First look at the event rules. Make sure you are aware of the competition regulations. Things like size and weight, weapon restrictions (most restrict untethered projectile weapons and flammable fluids for example), control requirements (dead mans switch or safety key to cut power). Be very clear about the scale of the robot you are building - is it a great huge car sized monster, or little ant weight, or something in-between - this knowledge is essential to the core of your design.
</p>
<p>Read our <a href="/wiki/robot_building_safety.html" title="Building robots can be dangerous - tips to help your safety">Robot Building Safety</a> guide.
</p>
<p>Also make sure you are aware of what constitutes a win - you need to make sure you design your robot for that goal.
</p>
<p>Scope out the competition - what kind of weapons are already in use, what is or is not successful, what weights/sizes are they? You might get some good ideas - and many are fairly sportsmanlike - but don't expect them to give away their top secrets. If this is a series event - Ie there have been previous ones, watch them and get a gist for it.
</p>
<h1  id="Preparation_-_Design">Preparation - Design</h1>
<p>Now you should start the design of your robot. It is up to you how far you take it, napkin drawings or full <a href="/wiki/cad.html" title="Computer Aided Design">CAD</a>/CAM simulations.
</p>
<p>I suggest a Brainstorm Session - getting all the ideas out first, then actually brainstorming each individual idea - weighing up its pros and cons and fleshing them out. Once you have settled on some ideas - you are prepared to make drawings.
</p>
<p>Some inspiration:
</p>
<ul><li><a href="/wiki/robot_locomotion.html" title="Robot Locomotion">Robot Locomotion</a>
</li><li><a href="/wiki/battle_robot_weapons.html" title="Battle Robot Weapons">Battle Robot Weapons</a>
</li><li>Battle Robot Armour
</li></ul><p>
<br/>Just make sure you have a clear image of what your robot is going to look like, work like before you build it. Cost up and make sure your parts are available. Personally - I love salvaged parts - see my <a href="/wiki/salvage_tips.html" title="Tips on pulling stuff apart to build robots. How, where and what.">Salvage Tips</a> for more. Watch <a href="/wiki/scrapheap_challenge.html" title="Quirky British Gameshow">Scrapheap Challenge</a> for some serious inspiration in salvage. With salvaged parts, sometimes you need to think a bit more about the general shape and function of a part, and leave a bit of leeway in the design for that.
</p>
<p>Design in a modular fashion to make task managing simpler - discrete goals along the way. This means you can test an analyse each one separately, and having the discrete goals gives you milestones - something that can really boost team motivation. As you design each module - you may think of a simple test rig, and cost and design this too. Unit Testing is great for program subroutines, single electronic chips and boards, gearboxes (make sure the outputs do what you expect them to) and structures (can it really withstand the stresses you thought it could?).
</p>
<p>As part of the design, think about what tools and safety gear you are going to need, and if you can realistically get access to them. If you need to purchase or hire tools and a workshop/garage, be sure to include that in your cost. Make sure to leave a bit of slack in the costing - these things tend to end up more expensive than they start off. Of course - you can also be economical - garden sheds tend to make great workshops.
</p>
<p>By the end of this phase - you should have no loose ends, and every phase of your build should be clear.
</p>
<h2  id="Components_you_should_consider">Components you should consider</h2>
<p>Drive system - What kind of wheels or tracks you will use, how many, how big - both diameter, tread width and tread depth. You need to make sure you can get enough traction - style is a factor, as well as clearance (more on that later). Also - don't go overboard - can you really build link tread for a tracked system? in the time allotted? Wheelchair, go-kart and lawnmower wheels make good salvage for this, or in ant weights - <a href="/wiki/lego.html" title="The best known construction toy">Lego</a> wheels are great. Don't try to work without tyres - otherwise you will have bad acceleration, bad steering and little pushing power. For tracks - you may be able to find some (good luck), but it is more likely you will have to build them. Consider well hinged links, with rubber (or leather if that is easier to get) boots across them to give traction. Metal plates alone have very little traction. Leather or rubber is easier to tool - if you want to give some character by putting a decal on every 9th tread or something.
</p>
<p>Drive power - On a large robot, you are probably going to use wheelchair motors or lawnmower motors. Make sure you have enough power to drive those- they are pretty high consumers. Next you have drill and jigsaw motors- which are both quite high-torque motors, and may conveniently coe with their own battery packs. On the ant weight, you could salvage basic DC motors from tooth-brushes and kids toys, or you could use converted/hacked servos - which means you get your PWM speed controller built in, and can probably lug it straight into your RC unit.
</p>
<p>What gearing or chain mechanisms are you gonna use? Bicycles and motorbikes can supply chains for larger bots, <a href="/wiki/lego.html" title="The best known construction toy">Lego</a> for small ones. What about gears? Again use car and bike scrap for huge gears, or <a href="/wiki/lego.html" title="The best known construction toy">Lego</a> for very small ones.
</p>
<p>Chassis and armour - for a large robot, sheet metal, mixed with cuts from car chassis makes for a sturdy and heavyweight design. Aluminium is slightly lighter, but more expensive, and less likely to be found in scrap, as are the hi-tech materials like polycarbonate. For ant-weights, you may still use very thin sheet metal, as well as plasticard and balsa wood for the non-impact areas. Try to get a frame work to hold the drive, weapon and control systems, leaving the outside for shelling with an armoured layer. Things like double skinned armour, or multi-material can be very effective. I have heard of leather armoured backed with a metal grid before - it was pretty effective - but had a weakness to piercing if you got in between the grid bars.
<br/>Make sure your chassis and drive design takes into account ground clearance - to little, and you will scrape and drag - making steering a nightmare, too much - and you are easy prey for the flipping wedges.
</p>
<p>Control systems - First you need an RC system, you might be able to yank one from a kids RC toy, but these are generally only two channel - and you may need more than one of them(on slightly different wavelengths). You could buy a system - but they are not cheap - however - they would be more reliable, and should be easier to interface than salvaged ones.
</p>
<p>The RC controller alone may not have enough juice for large motors, and generally output <a href="/wiki/servo_motor.html" title="A motor with built in positioning control - easily interfaced with digital systems">Servo motor</a> <a href="/wiki/pwm.html" title="Pulse Width Modulation">PWM</a> signals. You will need speed controllers to interface those to the motors.
</p>
<p>Don't forget batteries- these are going to be your main weight source, and also cna be a pain - so make sure you can replace them easily and that you are ble to get backup packs to an event. If you design, or use a really obscure battery system - you will find yourself in dire straights. Normally - you would use rechargeable packs, and you might get away with a couple of packs from cordless electric screwdrivers/drills. in smaller robots - you are probably going to use a 9v battery, but consider some mobile phone and PDA batteries might be suitable.
</p>
<p>Weapons - for these you would use one or more of those extra RC ports. Make sure you have a reasonably quick return mechanism - a weapon you cna only use once or twice is a bit useless. The weapon is one of those things that can really define a robots presence in a battle. Some go for the rather popular flipping system - which requires some fairly intense knowledge of pneumatics or hydraulics - however it could be done with a motor winding a spring-loaded catapult, and one RC channel pulling away the catch. Axes can also be done with pneumatics or wound springs. Slicing disks are relatively simple - but beware of contest rules. Spinning armour shells are effective, and impressive, but can be quite difficult to do - the whole of the armour would need bearings, and you need a pretty decent motor and gearing system to get a serious torque. Be aware of the spin-up times on these.
</p>
<p>Some robots go for more passive weapons - ramming spikes and such- which with more time and energy spent on a serious drive system, can be extremely effective.
</p>
<p>You can then spend thought on the painting or decal on the armour, and the name of the robot - which although you may already have one, might have changed as the design really gave character to the robot.
</p>
<p>Of course - these are only some suggestions, you may have a new or unique drive system, armour technique, power system, controller or weapon - excellent! Make sure you consider fully how you would build it, where the parts would come from, what its impacts are on the power, the weight of the robot, the frame and how you would mount it, whether it fits the rules of the competition, it you have enough time to build it, how serviceable/replaceable it is if it fails etc.
</p>
<h1  id="Preparation_-_Team_labour_division">Preparation - Team labour division</h1>
<p>Begin to divide the labour- who is good at what area, and start to plan how much time you are going to need to build it and plan it. Be sure to reserve enough time for potentially troublesome/difficult areas. You might have someone designing more specific issues, while other stuff is being built - but beware that they may have to rework it if the design requires it.
</p>
<p>Divide the build time into phases - and your design should be similarly modular, so people can function in a parallel way, and things will fit together in the end.
</p>
<p>Be aware of your skill sets here, even if that means simply asking people what they are good at, and what they would like to do. Giving people too much to do, or leaving people out in the cold is a surefire way to cause friction - so try to plan in a way that gives everyone something to do. although there are some exceptions, giving someone the same job over, and over can be very demotivating and soul-destroying. An example is never make the youngest member the tea-boy for the whole build, he will certainly not thank you for it.
</p>
<p>
</p>
<h1  id="Preparation_-_Tools_Safety">Preparation - Tools &amp; Safety</h1>
<p>Now you should get hold of the tools - and safety gear. Make sure to read the <a href="/wiki/robot_tools.html" title="Tools that are often required to get started in robot building">Robot Tools</a> page and the <a href="/wiki/robot_building_safety.html" title="Building robots can be dangerous - tips to help your safety">Robot Building Safety</a> page and have covered all risks. People tend to drive robots badly if they have just lost an eye in a welding accident.
</p>
<p>Never attempt to substitute a tool for the wrong job - not only are you likely to get a bad result, but this is quite a major safety hazard.
</p>
<p>You may want to buy/hire tools right when you need them - just make sure you are not caught short without them. And never be caught short without safety gear.
</p>
<p>In terms of working area, make sure that the flooring is suitable, and wont be easily damaged by dropping metal or you don't mind getting oil on it too much. A family living room is not suitable except maybe for ant weight robots.  Make sure you have enough room for the whole team to work when they need to - even if that means some parts being done in different peoples garages then pulled together later (this means having good modular design as discussed above). If you are limited for elbow room, tools become extremely dangerous.
</p>
<p>A gung-ho team member who flaunts safety recommendations and refuses to wear safety gear can be a real liability to the team - it is worth being fairly firm/stern on the subject. In my observations - girls do tend to be more sensible with regards to these things.
</p>
<p>It is better to call in help from team members, than to fall behind. It is better to fall behind than stay up till 3am every morning finishing it though. Tired builders are an accident waiting to happen - whether the victim will be you or the build is entirely a matter of luck - just don't take that risk.
</p>
<p>Try and finish each day by about 9-10pm, leaving yourself time to unwind. Frayed tempers also lead to accidents and schoolboy mistakes. I know it may sound very corpy, but sometimes the last 20 minutes are well spent sat together going over where the build has reached, what mistakes or inspirations people have had, and taking a general review of your progress. As well as airing new ideas (which may be miles better than you original ones), and ways out of tricky problems (which you will inevitably have a few) and also deal early with any team disputes.
</p>
<h1  id="Preparation_-_Acquiring_parts">Preparation - Acquiring parts</h1>
<p>This is ongoing - and may continue through the build - just make sure that all the parts for each build phase are ready before it starts.
</p>
<p><a href="/wiki/salvage_tips.html" title="Tips on pulling stuff apart to build robots. How, where and what.">Salvage Tips</a>
</p>
<p>While you can try and barter and save money on parts - remember quality parts do make for quality robots. Salvage parts of a quality brand or type may be be better than cheap parts or substitutions.
</p>
<p>Be sure to test your parts properly before putting them in the robot - and if you can, before you part with cash for them. If you are mail ordering parts - make sure you know the return conditions, and have ample time to test and return it if it is not up to scratch.
</p>
<p>There are specialist companies who actually machine parts for you from CAD designs, and although they are not cheap - the results are usually quite amazing.
</p>
<h1  id="The_fun_bit_-_The_build">The fun bit - The build</h1>
<p>At this stage you should have everything in place - the team, tools and design. Make sure you know how to use the tools, and achieve what the design sets out. It goes without saying to try and keep to plan with each phase.
</p>
<p>If there is an accident - be prepared to turn off the power immediately and deal with it. Safety is much more important than build time- I cannot stress this enough.
</p>
<p>Test every module - build sub assemblies, test them thoroughly with the test rigs you designed, measure them up to make sure they are going to fit. Then when you assemble the whole - you know each subpart is working, and this will save you a time consuming troubleshooting session later.
</p>
<p>Be prepared to alter your design as things come up, as I have already said, you will find things that worked in theory don't pan out in practice, and people sometimes just get a spark of inspiration while building. However - if that inspiration leads to shortcuts in safety or quality - you must still seriously consider their impact. If the new ideas are things in addition to the original concepts, then you may want to note them down, plan them for later, and make sure you at least satisfy the original requirement. Some things are best saved for "extra time" if you have it.
</p>
<p>Use our <a href="/wiki/building_tips.html" title="Hints and helpers for actually building robots, and other stuff.">Building Tips</a> page for specific tool and building ideas. And read our <a href="/wiki/robot_building_safety.html" title="Building robots can be dangerous - tips to help your safety">Robot Building Safety</a> page one more time just to make sure its all well instilled into your teams methodology.
</p>
<h1  id="Final_tests">Final tests</h1>
<p>Have a thorough test of every function, and every foreseeable event before the competition. Although you can guarantee to be surprised by something you didn't think of - have a good idea of your battery life, what kind of knocks the robot can take, the real distance of any remote controls, the kind of damage/force a weapon can exert.
</p>
<p>Make sure you can steer the robot - build yourselves a small slalom run to get used to it, and get a bit of old scrap to test the weapons out on. Try removing wheels(not all at once!) and other parts to see how well it runs - if you are damaged in a battle - you want to continue going as long as possible.
</p>
<p>Some very extreme teams actually build competing robot designs, and test them out against each other to select the one that goes into battle. This technique may also be used for individual modules. If you are entering successive competitions, then comparing modules or robots in the field is a good way of making sure your changes really are improvements.
</p>
<p>while I have put the testing section this far through, it is worth testing individual modules and components all the way through the build phase. The earlier your team is aware of a potential problem, the better chance you have of find a solution to it in the time you have.
</p>
<h1  id="The_day_of_the_battle">The day of the battle</h1>
<p>Make sure you have got new batteries in there, and your driver is well and confident. Make sure you are well rested. You need to perform as well as your robot - you need to be ready for any event in the arena and pits.
</p>
<p>GOOD LUCK! We look forward to hearing from builders and their experiences here!
</p>
<h1  id="Links">Links</h1>
<ul><li> <a  href="http://www.robot-4u.com/buildrobot/" rel="external" target="_blank">Robot 4u - Build Robots</a> - Links about building Robots
</li><li> <a  href="http://www.robot-4u.com/battlebot/" rel="external" target="_blank">Robot 4u - BattleBot</a> - Links about battlebots.
</li></ul>

