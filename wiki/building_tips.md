---
layout: page
title: Robot Building Tips
tags: [robot, robotics]
date: 2013-05-02 20:34:32
---
## Safety

[Robot Building Safety](/wiki/robot_building_safety.html "Building robots can be dangerous - tips to help your safety") - Important read this!

## Getting stuck into a floor roving robot

The Orion Explorer One Robot Building Kit is a good way to start off a robot project - it means that you will have all you need for a moving robot.  The Controller, motors, wheels, chassis, battery box are taken care of. A construction guide shows you how to put these parts together. Based on the popular Arduino, there is a huge amount of example code and hardware to use it with, and code on github to get basic functionality on day 1. There is no soldering here, only a screwdriver, batteries and a connected PC with a free USB port are needed.

## Next Steps

- [CAD](/wiki/cad.html "Computer Aided Design") and [Design](/wiki/design.html "Design") - Before you start a complicated build - ensure you design your robot well.
- Fixing A Broken Wire - When a wire is snapped in a Cybot or similar robot kit - how can you repair it?
- [Robot Tools](/wiki/robot_tools.html "Tools that are often required to get started in robot building") - What do you need for the job?
- [Damaged Screw Head](/{% post_url 2005-08-20-damaged-screw-head %} "Getting out that really stubborn screw or bolt") - Broken screw heads are tough - how can you get them out.
- [Retrieving Dropped Screws](/wiki/retreiving_dropped_screws.html "What to do when you drop a screw, nut or bolt into the gubbins of a machine") - Getting to that screw you dropped deep in the gubbins of your robot.
- [Choosing A Glue](/wiki/choosing_a_glue.html "Choosing the right glue, for the right job") - There are a number of glues you can use - which should you choose and why?
- [Printed Circuit Boards](/wiki/printed_circuit_boards.html "Printed Circuit Boards") - Sooner or later you will need to think about this for advanced circuit design.
- [Part Reuse](/wiki/part_reuse.html "Part Reuse") and [Modular Robot Design](/wiki/modular_robot_design.html "Modular Robot Design") are two areas that every robot builder should familiarise themselves with.
- [Lego](/wiki/lego.html "The best known construction toy") and [Construction Toys](/wiki/construction_toy.html "Construction Toy").
- [MicroControllers](/wiki/microcontroller.html) - This is a round up, including [PIC's](/wiki/pic.html "PIC"), the [Lego RCX](/wiki/rcx.html "The Lego RCX") and others.
- [Building A Battle Robot](/wiki/building_a_battle_robot.html "Building A Battle Robot").

This is a tip picked up from [Real Robots](/wiki/real_robots.html "Real Robots"). When using self-tapping screws - perform a quarter turn back towards the end to loosen away swarf (excess material) before continuing to avoid stiffness.

When using [PIC](/wiki/pic.html "PIC")'s and other chips - always try to use sockets for these. This means that when you have a fault, or wish to upgrade it, or reuse it - you can. Also - it means that you have much less chance of damaging the chip when soldering the socket. Many of these components are sensitive to heat.

If you are working with IC's and transistors, always try to make sure you are earthed - buy a wrist band from your local PC dealer.  Many of these devices are sensitive to electrostatic discharges.  That also means you should store them in their protective bags.  I also tend to find a sheet of paper or card, during work, is a good place to store them temporarily. You can briefly earth yourself by touching a metal part of a case - but using the band means you have both hands, and ensures proper contact.

Try to avoid using screwdrivers that are the wrong size for the screw- although it may be convenient and tempting to use one screwdriver for everything - you have a good chance of destroying the screw heads and screw driver.
