---
layout: page
title: Byte
tags: [computing, programming]
date: 2007-03-17 20:15:49
---
A byte is a common computing term given to 8 [bits](/wiki/bit.html "Binary Digit") of [binary](/wiki/binary.html "The storage method for digital information") data.

It is considered one of the main denominations of information transfer and storage in computing, and is what is referred to by the megabytes of [RAM](/wiki/ram.html "Random Access Memory") and the gigabytes of storage.

A single byte may represent one of 256 different states, usually numbered 0 through to 255.

In many systems, one character (letter, or symbol) is one byte's worth of data, the exception being in multilingual systems that need to hold more than 256 character (minus certain control characters) glyphs.
