---
layout: page
title: Deferred design
tags: [robot building, cad, programming, electronics]
date: 2005-10-06 18:08:00
---
Deferred design refers to a system with the ability to be modified or tweaked later.

It best demonstrated by using programmable MicroControllers or [FPGAs](/wiki/fpga.html "Field Programmable Gate Array") which are programmable. This means that they could be deployed in an application - a robot, and later programmed, or reprogrammed as is necessary. They would be able to develop a design further, and actually test in the field without the expense of recreating new hardware for each iteration.
