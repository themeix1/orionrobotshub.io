---
layout: page
title: Lego Technic
date: 2004-11-15 15:03:53
---
This range of [Lego](/wiki/lego.html "The best known construction toy") has been around from a very early stage in the companies expansion.

It is what makes Lego such a useful [Construction Toy](/wiki/construction_toy.html "Construction Toy"). It includes Wheels, Holed Beams, Axles, Connecting Pins, Universal Joints, Cogs (including complex clutches and worm gears), [Pneumatic](/wiki/pneumatic.html "Use of air to operate and power actuators") components (which the glaring omission of a [Solenoid](/wiki/solenoid.html "Solenoid") operated valve), motors, sensors and even computers.

It is Lego Technic which has started off a lot of hobbyist robot builders, and will continue to keep them in good stead for the future.
