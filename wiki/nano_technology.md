---
layout: page
title: Nano Technology
date: 2005-01-17 19:33:47
---
This is technology engineered to be smaller than visible to the human eye - Nano meaning Nanometer. Already motors, sensors and microcircuitry has been created on this scale, but not in any usable amount.

This technology will revolutionise our world - at first, chip fabrication will become easier, cheaper and more capable. Engineering and synthesis of chemicals like paints and polymers will become easier, and we will see great advances. Eventually, we will have [Nano Bots](/wiki/nano_bots.html "Microscopic or").

It is through nano tech that we will have items like a Star Trek replicator - producing usable material from waste or raw material extremely efficiently.

Some people are fearful of this, and worry that the Nano Tech devices will be more like Stargate:The Replicators. I see this as unlikely - as the intelligence needed to behave like these is a greater than that needed for manipulation of molecules - which is a repeatable blueprinted process.

Once we have nano-scale manipulators, recycling will also become an increasingly viable option in cost and efficiency.
