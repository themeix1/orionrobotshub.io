---
layout: page
title: NOT Gate
tags: [electronics, programming, computing]
date: 2006-04-23 12:07:22
---
The <a href="/wiki/boolean.html" title="Boolean">Boolean</a> NOT operation is a negation of its input.

That is, the output of a NOT gate is NOT its input.

Look at this <a href="/wiki/truth_table.html" title="Truth Table">Truth Table</a>

| In | Out |
| -- | --  |
| 0  | 1   |
| 1  | 0   |

In schematic logic diagrams this is represented as a small circle on an input, or output, or as a buffer (triangle) plus the small circle. It is also commonly represented in notation as an overscore on the negated terms.
