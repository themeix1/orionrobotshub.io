---
layout: page
title: OR
tags: [electronics, programming, computing]
date: 2006-04-23 00:35:43
---
OR is a [Boolean](/wiki/boolean.html "Boolean") operation which will be true when any of its inputs are 1\. That is (on a two input gate) when one input, OR another, OR both are true - the output is true.

Consider the truth table for this:

Inputs |       | Output
------ | ----- | ------
**A**  | **B** | **C**
0      | 0     | 0
0      | 1     | 1
1      | 0     | 1
1      | 1     | 1
