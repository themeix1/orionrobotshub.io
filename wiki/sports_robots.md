---
layout: page
title: Sports Robots
tags: [robots]
date: 2005-02-04 11:28:06
---
Robots in use appear to be popping up everywhere. Including sports. As well as things as extravagant as the [Robo Cup](/wiki/robo_cup.html "Robo Cup"), robots are also used for training players.

## Conventional Sports With Robots

- [Tennis Robots](/wiki/tennis_robots.html "Tennis Robots")
- [Robo Cup](/wiki/robo_cup.html "Robo Cup")
- Dancing Robots

## New Sports Designed With Robots

- [Robot Wars](/wiki/robot_wars.html "The british robot smashing TV series.") - This really could be seen as robot pugilism
