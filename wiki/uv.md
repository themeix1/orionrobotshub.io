---
layout: page
title: UV - Ultra violet
tags: [tools]
date: 2005-06-29 22:55:44
---
This is sometimes used as part of processes to build robots.

It is used in the manufacture of [PCB's](/wiki/pcb.html "Printed Circuit Board") when use photoresist. It is also used to clear older [EPROM](/wiki/eprom.html "Erasable Programmable Rom") chips, and can still be used to do the same with [EEPROM](/wiki/eeprom.html "Electrically Erasable Programmable ROM") chips.

It is not normally used in sensor technology - as it is considered more harmful or intrusive than [IR](/wiki/ir.html "Acronym for Infra Red"). However - it can be used to reveal things not normally visible in ambient light. It is not difficult to see a robot or machine using UV to check the validity of monetary notes.
