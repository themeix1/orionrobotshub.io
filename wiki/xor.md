---
layout: page
title: XOR
tags: [electronics, computing, programming]
---

XOR means eXclusive Or.  It is a <a href="/wiki/boolean.html" title="Boolean">Boolean</a> operation which takes binary inputs and combines them for a single digital output.

An XOR gate is explicitly a 2 input device, who's output is only true if and only if the inputs are different.

Look at the [Truth Table](truth_table.html):

| Input |       | Output |
|-------|-------|--------|
| __A__ | __B__ | __C__  |
|   0   |   0   |    0   |
|   0   |   1   |    1   |
|   1   |   0   |    1   |
|   1   |   1   |    0   |
